// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package bishodroid.com.rapidkl_busdriver.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import bishodroid.com.rapidkl_busdriver.R;

public class Dialogs {

    private static AlertDialog.Builder dialog;


    /**
     * Dialog is shown when user's GPS is disabled
     *
     * @param context
     */
    public static void showGpsDialog(final Context context) {
        dialog = (new AlertDialog.Builder(context)).setTitle(R.string.title_enable_gps)
                .setMessage(R.string.msg_enable_gps)
                .setPositiveButton(R.string.dialog_enable, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }

                }).setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }

                });
        dialog.create().show();
    }

    /**
     * Dialog s show when user is not connected to the internet
     *
     * @param context
     */
    public static void showNetworkDialog(final Context context) {
        dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.title_enable_internet)
                .setMessage(R.string.msg_enable_network)
                .setPositiveButton(R.string.dialog_enable, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialoginterface, int i) {
                        context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }

                }).setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }

                });
        dialog.create().show();
    }

    public static void showMessageDialog(Context context, String title, String message) {
        dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        dialog.create().show();
    }
}
