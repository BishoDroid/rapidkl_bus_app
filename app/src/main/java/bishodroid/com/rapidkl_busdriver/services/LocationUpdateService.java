package bishodroid.com.rapidkl_busdriver.services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.util.Date;

import bishodroid.com.rapidkl_busdriver.pojo.Coordinates;
import bishodroid.com.rapidkl_busdriver.utils.AppConnectionHandler;
import bishodroid.com.rapidkl_busdriver.utils.Constants;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;

public class LocationUpdateService extends IntentService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String WSURI = "ws://echo.websocket.org";

    private Location currentLocation;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private String updateTime;
    private WebSocketConnection socket;
    private Gson gson;
    private Coordinates current = new Coordinates();

    public LocationUpdateService() {
        super(LocationUpdateService.class.getName());
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        startLocationUpdates();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
        googleApiClient.disconnect();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildGoogleApiClient();
        socket = new WebSocketConnection();
        connectToServer(socket);
        gson = new Gson();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i("Bus Driver", "Connected to GoogleApiClient");
        try {
            currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            updateTime = DateFormat.getTimeInstance().format(new Date());
        } catch (Exception ex) {
            Log.e("Bus Driver", ex.getMessage());
        }
        if (currentLocation != null) {
            notifyChannel(currentLocation, true);
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("Bus Driver", "Connection suspended");
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        updateTime = DateFormat.getTimeInstance().format(new Date());
        publishLocation(location, updateTime);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //TODO check connection and GPS and notify user
    }

    /**
     * Build google api
     */
    private void buildGoogleApiClient() {
        Log.i("Bus Driver", "Building GoogleApiClient...");
        googleApiClient = (new GoogleApiClient.Builder(this))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
        googleApiClient.connect();
    }

    /**
     * Create a location request
     */
    private void createLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(Constants.UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(Constants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Connects the app to the ws server
     *
     * @param socket
     */
    private void connectToServer(WebSocketConnection socket) {
        try {
            socket.connect(WSURI, new AppConnectionHandler());
        } catch (WebSocketException e) {
            e.printStackTrace();
        }

    }

    /**
     * Starts the location updates
     */
    public void startLocationUpdates() {
        try {
            if (googleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            else {
                googleApiClient.connect();
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            }

        } catch (SecurityException securityexception) {
            Log.e("Bus Driver", securityexception.getMessage());
        }
    }

    /**
     * Stops the location updates
     */
    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        notifyChannel(currentLocation, false);
    }


    /**
     * Notifies the Autobahn channel that this instance had just started
     * the location update service
     *
     * @param location
     */
    private void notifyChannel(Location location, boolean start) {
        if (start)
            Toast.makeText(this, "Just started at: " + Double.toString(location.getLatitude()) + " : " + Double.toString(location.getLongitude()),
                    Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "Just stopped at: " + Double.toString(location.getLatitude()) + " : " + Double.toString(location.getLongitude()),
                    Toast.LENGTH_LONG).show();
    }

    /**
     * Method to publish location updates to server via Autobahn web socket
     *
     * @param location
     * @param updateTime
     */
    private void publishLocation(Location location, String updateTime) {
        Toast.makeText(this, "New Location: " + Double.toString(location.getLatitude()) + " : " + Double.toString(location.getLongitude())
                , Toast.LENGTH_SHORT).show();
        current.setLatitude(location.getLatitude());
        current.setLongitude(location.getLongitude());
        current.setUpdateTime(updateTime);
        String json = gson.toJson(current);
        if (socket.isConnected()) {
            socket.sendTextMessage(json);
        } else {
            connectToServer(socket);
            socket.sendTextMessage(json);
        }
    }

}
