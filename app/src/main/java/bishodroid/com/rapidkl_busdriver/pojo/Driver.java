package bishodroid.com.rapidkl_busdriver.pojo;

import java.io.Serializable;

/**
 * Created by Bisho on 21/2/2016.
 */
public class Driver implements Serializable {

    private String name;
    private String route;
    private String area;
    private String photoUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", route='" + route + '\'' +
                ", area='" + area + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                '}';
    }
}
